FROM tomcat:8-jdk8-adoptopenjdk-hotspot
MAINTAINER Walid MANSIA <walid.mansia@gmail.com>

ADD apache-tomcat-8.5.61/webapps/birt /usr/local/tomcat/webapps/ROOT
ADD apache-tomcat-8.5.61/conf/tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml